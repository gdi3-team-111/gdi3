#include "Tree.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <windows.h>

#define ROT 1
#define SCHWARZ 0

using namespace std;

Tree::Tree()
{
    //ctor
    root = NULL;
}

Tree::~Tree()
{
    //dtor
    freeNodes(root);
}

//Neuen Knoten mit gegebenem/r Key und Info
void Tree::treeInsert(int key, Info *inf){

    Node *y = NULL; //PFADZEIGER
    Node *x = this->root; //Wurzel zwischenspeicher
    Node *z = new Node; //InsertNode
    z->key = key;
    z->left = nullptr;
    z->right = nullptr;
    z->info = inf;
    z->farbe = ROT;

    while(x!=NULL){
        y = x;
        if (z->key < x->key)
            x = x->left;
        else
            x = x->right;
    }
    z->parent = y;
    if(y == NULL)
        root = z;
    else if (z->key < y->key)
        y->left = z;
    else
        y->right = z;


    rb_insert_fixup(z);
}

//Fixup nach einem Insert zur Einhaltung der RB-Eigenschaften
void Tree::rb_insert_fixup(Node *z){
    Node *y;
    while (z != root && z->parent->farbe == ROT){
        if (z->parent == z->parent->parent->left){

            y = z->parent->parent->right;

            if (y != nullptr && y->farbe == ROT){
                z->parent->farbe = SCHWARZ;
                y->farbe = SCHWARZ;
                z->parent->parent->farbe = ROT;
                z = z->parent->parent;
            }
            else{
                if (z == z->parent->right){
                    z = z->parent;
                    left_Rotate(z);
                }
                z->parent->farbe = SCHWARZ;
                z->parent->parent->farbe = ROT;
                right_Rotate(z->parent->parent);
            }
        }
        else {

            y = z->parent->parent->left;

            if (y != nullptr && y->farbe == ROT){
                z->parent->farbe = SCHWARZ;
                y->farbe = SCHWARZ;
                z->parent->parent->farbe = ROT;
                z = z->parent->parent;
            }
            else{
                if (z == z->parent->left){
                    z = z->parent;
                    right_Rotate(z);
                }
                z->parent->farbe = SCHWARZ;
                z->parent->parent->farbe = ROT;
                left_Rotate(z->parent->parent);
            }
        }
    }
    this->root->farbe = SCHWARZ;
}

//Rotationen
void Tree::left_Rotate(Node *x){

    Node *y = x->right;
    x->right = y->left;

    if (y->left != nullptr)
        y->left->parent = x;

    y->parent = x->parent;

    if (x->parent == nullptr)
        this->root = y;
    else if (x == x->parent->left)
        x->parent->left = y;
    else
        x->parent->right = y;

    y->left = x;
    x->parent = y;

}

void Tree::right_Rotate(Node *x){
    Node *y = x->left;

    x->left = y->right;

    if (y->right != nullptr)
        y->right->parent = x;

    y->parent = x->parent;

    if (x->parent == nullptr)
        this->root = y;
    else if (x == x->parent->right)
        x->parent->right = y;
    else
        x->parent->left = y;

    y->right = x;
    x->parent = y;

}

//Search Methoden
//public-Methode ruft private Methode auf
struct Info *Tree::treeSearch(int k){

    return treeSearch(k,root);
}

struct Info *Tree::treeSearch(int k, Node *x){

    if (x == NULL || k == x->key)
        return x->info;
    if (k < x->key)
        return treeSearch(k,x->left);
    else
        return treeSearch(k,x->right);


}


//Ausgabe
//Durch ein- und auskommentieren kann ausgew�hlt werden ob InorderAusgabe oder Strukturausgabe
void Tree::treePrint(){
    int tiefe = 0;              //Tiefe 0 = Start bei Wurzel
    if(this->root == NULL)
        printf("Der Baum existiert nicht oder ist leer!");
    else
        treePrintLeftToRight(root,tiefe);
    //InorderTreeWalk(root);
}

//Ausgabe der Keys inOrder
void Tree::inorderTreeWalk(Node *x){
    if (x != NULL)
    {
        inorderTreeWalk(x->left);
        printf ("%d ",x->key);
        inorderTreeWalk(x->right);
    }

}

//Farbige und Strukturierte Ausgabe
void Tree::treePrintLeftToRight(Node *root,int tiefe){

    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    int abstand = 1;

    if (root == NULL)
        return;

    tiefe += abstand;

    //R�ckw�rts Inorder da bei 90� Drehung der rechte Teilbaum in der obersten Zeile stehen soll
    treePrintLeftToRight(root->right,tiefe);

    printf("\n");

    for (int i = abstand; i < tiefe; i++){
        printf ("\t");
    }

    if (root->farbe == ROT){
        SetConsoleTextAttribute(hConsole,FOREGROUND_RED);
        printf("%d %d\n",root->key,root->farbe);
    }
    else{
        SetConsoleTextAttribute(hConsole,FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED);
        printf("%d %d\n",root->key,root->farbe);
    }

    treePrintLeftToRight(root->left,tiefe);
}

//Tree Tiefe
int Tree::treeDepth(){
    return treeDepth(root);
}

int Tree::treeDepth(Node *x){
    if (x == NULL)
        return 0;
    else {
        int left = treeDepth(x->left);
        int right = treeDepth(x->right);

        if (left >= right)
            return left+1;
        else
            return right+1;
    }
}

//Tree Gr��e
int Tree::treeSize(){
    return treeSize(root);
}

int Tree::treeSize(Node *x){
    if (x == NULL)
        return 0;
    else {
        int left = treeSize(x->left);
        int right = treeSize(x->right);
        int treesize = left + right + 1;
        return treesize;
    }
}

//Speicher des Baums rekursiv freigeben f�r Destruktor
void Tree::freeNodes(Node *x){
    if (x != NULL){
        freeNodes(x->left);
        freeNodes(x->right);

        delete x;
    }

}



