#ifndef TREE_H
#define TREE_H

#include <string>

using namespace std;

struct Info{
    char info[20];

    Info(char inf[]){
        for (int i = 0; i<20;i++){
            info[i]=inf[i];
        }
    }
};


class Tree
{
    private:

        //Data
        struct Node{
            Node* parent, *left, *right;
            int key;
            int farbe;
            Info *info;

        };
        Node* root;

        //Functions
        void freeNodes(Node *x);                            //F�r Destruktor
        int treeSize(Node *x);
        int treeDepth(Node* x);

        void inorderTreeWalk(Node* x);                      //Druckt die Keys Inorder aus
        void treePrintLeftToRight(Node *root,int tiefe);    //Druckt den Tree in Form und 90� nach links gedreht

        struct Info *treeSearch(int k,Node *x);             //N�tig f�r TreeSearch

        //Rotationen und Fixup
        void rb_insert_fixup(Node *z);
        void left_Rotate(Node *x);
        void right_Rotate(Node *x);


    public:
        Tree();
        virtual ~Tree();

        //Functions
        int treeSize();
        int treeDepth();

        void treePrint();
        void treeInsert(int key, Info *inf);
        struct Info* treeSearch(int k);

    protected:

};

#endif // TREE_H
