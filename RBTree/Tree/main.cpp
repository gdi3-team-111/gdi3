#include <iostream>
#include <stdio.h>
#include "Tree.h"
#include <stdlib.h>
#include <ctime>
#include <fstream>
#include <string>
//#include "Tree.cpp" //F�r Visual Studio

#define ANZAHL 100
using namespace std;

//Prototypen der Testfunktionen
void inserttwenty(Tree *baum,Info *info);
void insertoneduplicate(Tree *baum, Info *info);

void searchfive(Tree* baum);
void searchnonexisting(Tree* baum);

void checkdepthandsize(Tree* baum, Info* info);
void testaufgabe(Tree* baum, Info* info);

void getruntimerandominsert(Tree* baum, Info* info);
void getruntimesearch(Tree* baum, Info*info);

void insertfromdb(Tree* baum, Info* info);

int main()
{

    //Instanzerstellungen und Initialisierung
    Tree *baum = new Tree();
    char inf[20] ="test";
    Info *info = new Info(inf);
    srand(time(NULL));

    //Tests �ber die Prototypen ein- bzw. auskommentieren

//    inserttwenty(baum,info);
//    insertoneduplicate(baum,info);

//    searchfive(baum);
//    searchnonexisting(baum);

//    checkdepthandsize(baum,info);
//    testaufgabe(baum,info);

//    getruntimerandominsert(baum,info);
//    getruntimesearch(baum,info);

//    insertfromdb(baum, info);


//    baum->treePrint();

    return 0;
}

//Testfunktionen

//Test mit 20 Zufallzahlen und Einhaltung der RB-Eigenschaften
void inserttwenty(Tree *baum, Info *info){
    for (int i = 0; i < 20; i++){
        int randomnumber = rand();
        cout<<randomnumber<<endl;
        baum->treeInsert(randomnumber,info);
    }

    cout<<endl;
    baum->treePrint();
}

//Test mit 18 Zufallszahlen und einer Doppelten
void insertoneduplicate (Tree *baum, Info* info){

    baum->treeInsert(2550,info);
    cout<<2550<<endl;

    for (int i = 0; i < 18; i++){
        int randomnumber = rand();
        cout<<randomnumber<<endl;
        baum->treeInsert(randomnumber,info);
    }

    baum->treeInsert(2550,info);
    cout<<2550<<endl;

    baum->treePrint();
}

//Suche nach 5 aus 20
void searchfive (Tree* baum){

    char inf[20] = "nicht gesucht";
    Info *info = new Info(inf);
    char infs[20] = "gesucht";
    Info *infos = new Info (infs);

    baum->treeInsert(100,info);
    baum->treeInsert(90,info);
    baum->treeInsert(110,info);
    baum->treeInsert(0,infos);          //gesucht
    baum->treeInsert(200,info);
    baum->treeInsert(80,info);
    baum->treeInsert(120,info);
    baum->treeInsert(10,infos);          //gesucht
    baum->treeInsert(190,info);
    baum->treeInsert(70,info);
    baum->treeInsert(130,info);
    baum->treeInsert(20,infos);          //gesucht
    baum->treeInsert(180,info);
    baum->treeInsert(60,info);
    baum->treeInsert(140,info);
    baum->treeInsert(30,infos);          //gesucht
    baum->treeInsert(170,info);
    baum->treeInsert(50,info);
    baum->treeInsert(150,info);
    baum->treeInsert(40,infos);          //gesucht

    cout<<"Es gibt 5 gesuchte Keys, jeder davon weist im Info Attribut das Wort GESUCHT auf.\nTaucht ein NICHT GESUCHT im Ergebnis auf, wurde ein anderer Key gefunden."<<endl<<endl;

    for (int i = 0;i < 50; i=i+10){
        Info *ergebnis = baum->treeSearch(i);
        printf ("Ergebnis fuer die Suche von %d: ",i);
        for (int i = 0; i < 20; i++){
            printf ("%c",ergebnis->info[i]);
        }
        printf ("\n");
    }
}

//Suche nach nicht existierendem KEY
void searchnonexisting(Tree *baum){
    char inf[20] = "nicht gesucht";
    Info *info = new Info(inf);
    char infs[20] = "gesucht";
    Info *infos = new Info (infs);

    baum->treeInsert(100,info);
    baum->treeInsert(90,info);
    baum->treeInsert(110,info);
    baum->treeInsert(0,infos);          //gesucht
    baum->treeInsert(200,info);
    baum->treeInsert(80,info);
    baum->treeInsert(120,info);
    baum->treeInsert(10,infos);          //gesucht
    baum->treeInsert(190,info);
    baum->treeInsert(70,info);
    baum->treeInsert(130,info);
    baum->treeInsert(20,infos);          //gesucht
    baum->treeInsert(180,info);
    baum->treeInsert(60,info);
    baum->treeInsert(140,info);
    baum->treeInsert(30,infos);          //gesucht
    baum->treeInsert(170,info);
    baum->treeInsert(50,info);
    baum->treeInsert(150,info);
    baum->treeInsert(40,infos);          //gesucht

        printf ("Ergebnis fuer die Suche von 160: ");
        Info *ergebnis = baum->treeSearch(160);
        for (int i = 0; i < 20; i++){
            printf ("%c",ergebnis->info[i]);
        }
}


//�berpr�fung Tiefe und Groesse nach dem Einf�gen von 35 Keys
void checkdepthandsize(Tree* baum, Info* info){

    for (int i = 0; i < 35; i++){
        int randomnumber = rand();
        baum->treeInsert(randomnumber,info);
    }

    baum->treePrint();
    cout<<"Baumhoehe: "<<baum->treeDepth()<<endl;
    cout<<"Baumgroesse: "<< baum->treeSize()<<endl<<endl;

}


//Test mit RB-Baum aus den Aufgaben
void testaufgabe(Tree* baum, Info* info){

    baum->treeInsert(41,info);
    baum->treeInsert(38,info);
    baum->treeInsert(31,info);
    baum->treeInsert(12,info);
    baum->treeInsert(19,info);
    baum->treeInsert(8,info);
    baum->treeInsert(40,info);
    baum->treeInsert(39,info);
    baum->treePrint();
}

//Laufzeitmessung Insert : srand() in der main
void getruntimerandominsert(Tree* baum, Info* info){

    clock_t start, ende, zeit;
    start = clock();
    for (int i = 0; i < ANZAHL; i++){
        int randomnumber = rand();
        baum->treeInsert(randomnumber,info);
    }
    ende = clock();
    zeit = ende - start;


    printf("Zeit bei %d Elementen = %f Sekunden.",ANZAHL,(float)zeit/CLOCKS_PER_SEC);

}

//Laufzeitmessung Search
void getruntimesearch(Tree* baum, Info* info){

    clock_t start,ende,zeit;
    for (int i = 1; i <= ANZAHL; i++){
        baum->treeInsert(i,info);
    }


    start = clock();
    Info* ergebnis = baum->treeSearch(ANZAHL);
    ende = clock();

    zeit = ende - start;

    printf("\n");
    printf("Zeit f�r die Suche des Keys %d bei %d eingefuegten Keys = %f Sekunden.",ANZAHL,ANZAHL,(float)zeit/CLOCKS_PER_SEC);

}

//Insert DB-Daten + Laufzeit
void insertfromdb(Tree* baum, Info* info){

    //Einlesen Text Datei

    string filename = "db-netz-20200217-nodes.txt";
    int entry;
    int anzahl = 0;
    clock_t start,ende,zeit;

    start = clock();
    ifstream file (filename);
    if (file.is_open()){
        while ((file >> entry) && (anzahl < ANZAHL)){ //F�r die Begrenzung einer bestimmten Anzahl

            baum->treeInsert(entry,info); //Einf�gen
            anzahl++;                     //wenn anzahl == ANZAHL Abbruch Schleife
        }
        cout << anzahl << " Elemente wurden in den Baum hinzugef�gt!"<<endl;
        file.close();
    }
    ende = clock();
    zeit = ende - start;
    printf ("Einfuegen von %d Elementen = %f Sekunden. \n",anzahl,(float)zeit/CLOCKS_PER_SEC);

}
