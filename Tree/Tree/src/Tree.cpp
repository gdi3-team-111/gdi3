#include "Tree.h"
#include <stdio.h>
#include <iostream>

using namespace std;

/*
Baum Konstruktor
*/
Tree::Tree()
{
    root = NULL;
}

/*
Baum destruktor
*/
Tree::~Tree()
{
    FreeNodes(root);
}

/*
fuegt einen neuen Knoten zu mithilfe des Schlüssel wertes
*/
void Tree::TreeInsert(int key, Info *inf){

    Node *y = NULL; //PFADZEIGER
    Node *x = root; //Wurzel zwischenspeicher
    Node *z = new Node; //InsertNode
    z->key = key;
    z->info = inf;
    z->parent = nullptr;
    z->left = nullptr;
    z->right = nullptr;

    if (x == NULL){
        root = z;
    }
    else {
        while (x != NULL){
            y = x;
            if (z->key < x->key)
                x = x->left;
            else
                x = x->right;
        }

        z->parent = y;
        if (y == NULL)
            root = z;
        else if (z->key < y->key)
            y->left = z;
        else
            y->right = z;
        }
}


/*
korrigiert den Baum nach entfernen eines Knoten
*/
void Tree::Transplant(Node* u,Node *v){
    if(u->parent == nullptr)
        this->root = v;

    else if (u == u->parent->left)
        u->parent->left = v;

    else u->parent->right = v;

    if (v != nullptr)
        v->parent = u->parent;
}

/*
Methode zum entfernen einzelner Knoten
*/
void Tree::TreeDelete(int k){
    Node *y;
    Node *z = GetNode(k);

    if(z){
        if (z->left == nullptr)
            this->Transplant(z,z->right);

        else if (z->right == nullptr)
            this->Transplant(z,z->left);

        else{
            y = Tree_Minimum(z->right);
            if (y->parent != z){
                this->Transplant(y,y->right);
                y->right = z->right;
                y->right->parent = y;
            }
            this->Transplant(z,y);
            y->left = z->left;
            y->left->parent = y;
        }
    delete z;
    }
}

/*
Ruft mit Hilfe von Key und root die zweite GetNode Methode auf
*/
 Tree::Node* Tree::GetNode(int key){
    return GetNode(key,root);
};

/*
Rekursive Funktion die nach dem gesuchten Knoten sucht
*/
 Tree::Node *Tree::GetNode (int key, Node *x){
    if (x->key == key)
        return x;
    else
    {
        if(key < x->key)
            return GetNode(key,x->left);
        else
            return GetNode(key,x->right);
    }

}

/*
public-Methode ruft private Methode auf
*/
struct Info *Tree::TreeSearch(int k){
    return TreeSearch(k,root);
}

/*
Rekursive Search funktion die, den Info teil des Knoten wiedergibt.
*/
struct Info *Tree::TreeSearch(int k, Node *x){
    if (x == NULL || k == x->key)
        return x->info;
    if (k < x->key)
        return TreeSearch(k,x->left);
    else
        return TreeSearch(k,x->right);
}


/*
gibt die Knoten in reihenfolge fuer Treeprint aus
*/
void Tree::InorderTreeWalk(Node *x){
    if (x != NULL)
    {
        InorderTreeWalk(x->left);
        printf ("%d ",x->key);
        InorderTreeWalk(x->right);
    }

}

/*
ueberprueft ob der Baum existiert und ruft dann TreePrintLeftToRight auf
Für Inorder Ausgabe 170 auskommentieren 171 einkommentieren
*/
void Tree::TreePrint(){
    int tiefe = 0;              //Tiefe 0 = Start bei Wurzel
    if(this == NULL)
        printf("Der Baum existiert nicht oder ist leer!");
    else
        TreePrintLeftToRight(root,tiefe);
    //InorderTreeWalk(root);
}

/*
Gibt den Baum aus mitilfe der schluessel aus
*/
void Tree::TreePrintLeftToRight(Node *root,int tiefe){
    int abstand = 1;

    if (root == NULL)
        return;

    tiefe += abstand;

    //Rueckwaerts Inorder da bei 90 Grad Drehung der rechte Teilbaum in der obersten Zeile stehen soll
    TreePrintLeftToRight(root->right,tiefe);

    printf("\n");

    for (int i = abstand; i < tiefe; i++){
        printf ("\t");
    }

    printf("%d\n",root->key);

    TreePrintLeftToRight(root->left,tiefe);
}

/*
Public Funktion ruft die private TreeDepth auf
*/
int Tree::TreeDepth(){
    return TreeDepth(root);
}

/*
rekursive Funktion die die Tiefe des Baumes zaehlt
*/
int Tree::TreeDepth(Node *x){
    if (x == NULL)
        return 0;
    else {
        int left = TreeDepth(x->left);
        int right = TreeDepth(x->right);

        if (left >= right)
            return left+1;
        else
            return right+1;
    }
}

/*
Public Funktion die die Private Funktion aufruft
*/
int Tree::TreeSize(){
    return TreeSize(root);
}

/*
Zaehlt die Knoten des Baumes.
*/
int Tree::TreeSize(Node *x){
    if (x == NULL)
        return 0;
    else {
        int left = TreeSize(x->left);
        int right = TreeSize(x->right);
        int treesize = left + right + 1;
        return treesize;
    }
}

/*
Speicher des Baums rekursiv freigeben fuer Destruktor
*/
void Tree::FreeNodes(Node *x){
    if (x != NULL){
        FreeNodes(x->left);
        FreeNodes(x->right);

        delete x;
    }

}

/*
Tauscht aktuellen Knoten mit aktueller->rechts->links Knoten
*/
void Tree::Left_Rotate(int k){
    Node *x = GetNode(k);

    Node *y = x->right;
    x->right = y->left;

    if(y->left != nullptr)
        y->left->parent = x;

    y->parent = x->parent;

    if(x->parent == nullptr)
        this->root = y;
    else if(x == x->parent->left)
        x->parent->left = y;
    else
        x->parent->right = y;

    y->left = x;
    x->parent = y;
}

/*
Tauscht aktuellen Knoten mit aktueller->links->rechts Knoten
*/
void Tree::Right_Rotate(int k){
    Node *x = GetNode(k);

    Node *y = x->left;
    x->left = y->right;

    if(y->right != nullptr)
        y->right->parent = x;

    y->parent = x->parent;

    if(x->parent == nullptr)
        this->root = y;
    else if(x == x->parent->right)
        x->parent->right = y;
    else
        x->parent->left = y;

    y->right = x;
    x->parent = y;
}
