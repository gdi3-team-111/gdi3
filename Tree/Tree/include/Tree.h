#ifndef TREE_H
#define TREE_H

struct Info{
    char info[20];

    Info(char inf[]){
        for (int i = 0; i<20;i++){
            info[i]=inf[i];
        }
    }
};


class Tree
{
    private:

        /*
        Struktur der einzelnen Knoten
        */
        struct Node{
            Node* parent, *left, *right;
            int key;
            Info *info;

        };
        Node* root;

        //Functions
        void Transplant(Node* u,Node *v);
        void FreeNodes(Node *x);
        int TreeSize(Node *x);
        int TreeDepth(Node* x);
        void InorderTreeWalk(Node* x);
        void TreePrintLeftToRight(Node *root,int tiefe);
        struct Info *TreeSearch(int k,Node *x);
        struct Node *GetNode (int key);
        struct Node *GetNode (int key, Node *x);

        /*
        gibt den Knoten aus mit der kleinsten schluessel element
        */
        struct Node* Tree_Minimum(Node* x)
        {
            while (x->left != nullptr)
                x = x->left;
            return x;
        };

        /*
        gibt den Knoten aus mit der groessten schluessel element
        */
        struct Node* Tree_Maximum(Node* x)
        {
            while (x->right!=nullptr)
            x = x->right;
            return x;
        };




    public:
        Tree();
        virtual ~Tree();

        //Memberfunctions
        int TreeSize();
        int TreeDepth();
        void TreePrint();
        void TreeInsert(int key, Info *inf);
        struct Info* TreeSearch(int k);
        void TreeDelete(int k);

        //Rotationen
        void Left_Rotate(int k);
        void Right_Rotate(int k);

    protected:

};

#endif // TREE_H
