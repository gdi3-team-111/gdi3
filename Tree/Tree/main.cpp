#include <iostream>
#include <stdio.h>
#include "Tree.h"
#include <stdlib.h>
#include <ctime>
#include <fstream>
#include <string>
//#include "Tree.cpp" //f�r Visual Studio


#define ANZAHL 1000000

using namespace std;

/*
Test Funktionen
ueberpruefung dass der Baum korrekt Funktioniert.
*/
void test_insert(Tree* Baum, Info* info);
void test_rotate(Tree* Baum, Info* info);
void test_search(Tree* Baum, Info* info, int k);
void test_depth(Tree* Baum, Info* info);
void test_size(Tree* Baum, Info* info);
void test_delete(Tree* Baum, Info* info, int k);

void Read_file(Tree* Baum, Info* info);
void GetRunTimeInsert(Tree* Baum, Info* info);

/*
Main Funktion startet Hier
*/
int main()
{
    //Instanzerstellungen und Initialisierung
    Tree *Baum = new Tree();
    char inf[20] = "test";
    Info *info = new Info(inf);
    srand(time(NULL));

    /*
    Test Funktionen
    */
//    test_insert(Baum,info);
//    test_rotate(Baum,info);
//    test_search(Baum,info,55);
//    test_depth(Baum,info);
//    test_size(Baum,info);
//    test_delete(Baum,info,7);

//    GetRunTimeInsert(Baum,info);

//    Read_file(Baum,info);

    return 0;
}

/*
Test ob das einfuegen richtig funktioniert
*/
void test_insert(Tree* Baum, Info* info)
{
    int key = 0;
    for(int i = 0; i < 10 ; i++)
    {
        key = rand()%100;
        if(key == 55 || key == 11 || key == 2)
            {
                key++;
            }
        cout << "key ist = " << key << endl;
        Baum->TreeInsert(key,info);
        Baum->TreePrint();
    }
}

/*
Test ob die Rotation richtig funktioniert
*/
void test_rotate(Tree* Baum, Info* info)
{
    int key = 0;
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(22,info);
    Baum->TreeInsert(23,info);
    Baum->TreeInsert(21,info);
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(77,info);
    Baum->TreeInsert(78,info);
    Baum->TreeInsert(76,info);
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(rand()%100,info);
    Baum->TreeInsert(rand()%100,info);

    Baum->Left_Rotate(77);
    cout<<endl;
    Baum->TreePrint();
    Baum->Right_Rotate(22);
    cout<<endl;
    Baum->TreePrint();
}

/*
Test ob richtig gesucht wird
*/
void test_search(Tree* Baum, Info* info, int k)
{
    int key = 0;
    for(int i = 0; i < 10 ; i++)
    {
        key = rand()%100;
        if(key == k)
            {
                key++;
            }
        Baum->TreeInsert(key,info);
    }

    char Richtig[20] = "gesucht";
    Info *richtig = new Info(Richtig);
    Baum->TreeInsert(k,richtig);
    Baum->TreePrint();
    Info *Ausgabe = Baum->TreeSearch(k);

    cout << "bei Key "<< k <<" ist die ausgabe = ";
    for(int i = 0; i < 20; i++)
    {
        printf("%c",Ausgabe->info[i]);
    }
    cout << endl;

    /*
    int size = Baum->TreeSize();
    int depth = Baum->TreeDepth();
    cout << "size ist = " <<  size << endl;
    cout << "depth ist = " << depth << endl;
    */
}

/*
Test ob die Funktion fuer die Tiefe Stimmt
*/
void test_depth(Tree* Baum, Info* info)
{
    int key = 0;
    for(int i = 0; i < 10 ; i++)
    {
        key = rand()%100;
        Baum->TreeInsert(key,info);
    }

    Baum->TreePrint();
    int Depth = Baum->TreeDepth();
    cout <<"Der Baum hat eine Tiefe von "<<Depth << endl;
}

/*
Test ob die Funktion fuer die Size stimmt
*/
void test_size(Tree* Baum, Info* info)
{
    int key = 0;
    for(int i = 0; i < 10 ; i++)
    {
        key = rand()%100;
        Baum->TreeInsert(key,info);
    }

    Baum->TreePrint();
    int Size = Baum->TreeSize();
    cout <<"der Baum ate eine Size von "<< Size << endl;
}

/*
Test ein Knoten richtig geloescht wird
*/
void test_delete(Tree* Baum, Info* info, int k)
{
    int key = 0;
    for(int i = 0; i < 10 ; i++)
    {
        key = rand()%100;
        if(key == k)
        {
            key++;
        }
        Baum->TreeInsert(key,info);
    }
    Baum->TreeInsert(7,info);

    Baum->TreePrint();
    cout << "entferne  key = "<< k <<endl;
    Baum->TreeDelete(k);
    Baum->TreePrint();
}

/*
Funktion zum einlesen eines Baum aus einer Datei
*/
void Read_file(Tree* Baum, Info* info)
{
    string filename = "db-netz-20200217-nodes.txt";
    int entry,anzahl = 0;
    clock_t start,ende,zeit;

    start = clock();
    ifstream file (filename);
    if(file.is_open())
    {
        while ((file >> entry) && (anzahl < ANZAHL)) //begrenzung einer bestimmten Anzahl
        {
        Baum->TreeInsert(entry,info);
        anzahl++;
        }
    cout << anzahl << " Elemente wurden im Baum eingefuegt !" << endl;
    file.close();
    }
    ende = clock();
    zeit = ende - start;
    cout << "Einfuegen von " << anzahl << " Zuffallszahlen: "<< (float)zeit/CLOCKS_PER_SEC <<" Sekunden" << endl;
}

/*
LaufZeitMessung fuer Insert
*/
void GetRunTimeInsert(Tree* Baum, Info* info)
{
    clock_t start,ende,zeit;
    start = clock();
    for (int i = 0; i < ANZAHL; i++)
    {
        int key = rand();
        Baum->TreeInsert(key,info);
    }
    ende = clock();
    zeit = ende - start;
    cout << "Einfuegen von " << ANZAHL << " Zuffallszahlen: "<< (float)zeit/CLOCKS_PER_SEC <<" Sekunden" << endl;
}
